import { HistoryService } from './../services/history.service';
import { MainService } from './../services/main.service';
import { Component, OnInit } from '@angular/core';
import { DataModel } from '../model/data_model';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  public slideOptsBanner = {
    slidesPerView: 1,
    autoplay: true,
  };
  public mainData: DataModel = {
    confirmed: { value: 0, detail: '' },
    recovered: { value: 0, detail: '' },
    deaths: { value: 0, detail: '' },
    lastUpdate: '',
  };
  public countryTop: any = [
    // tslint:disable-next-line: max-line-length
    { name: 'Indonesia', code: 'ID', data: { lastUpdate: new Date(), confirmed: { value: 0, detail: '' }, recovered: { value: 0, detail: '' }, deaths: { value: 0, detail: '' } } },
    // tslint:disable-next-line: max-line-length
    { name: 'China', code: 'CN', data: { lastUpdate: new Date(), confirmed: { value: 0, detail: '' }, recovered: { value: 0, detail: '' }, deaths: { value: 0, detail: '' } } },
    // tslint:disable-next-line: max-line-length
    { name: 'Malaysia', code: 'MY', data: { lastUpdate: new Date(), confirmed: { value: 0, detail: '' }, recovered: { value: 0, detail: '' }, deaths: { value: 0, detail: '' } } },
    // tslint:disable-next-line: max-line-length
    { name: 'Brunai Darussalam', code: 'BN', data: { lastUpdate: new Date(), confirmed: { value: 0, detail: '' }, recovered: { value: 0, detail: '' }, deaths: { value: 0, detail: '' } } },
    // tslint:disable-next-line: max-line-length
    { name: 'Thailand', code: 'TZ', data: { lastUpdate: new Date(), confirmed: { value: 0, detail: '' }, recovered: { value: 0, detail: '' }, deaths: { value: 0, detail: '' } } },
  ];

  public newest: any = [];
  constructor(
    private mainSrv: MainService,
    private historySrv: HistoryService
  ) { }

  ngOnInit() {
    this.doRefresh();
  }

  async doRefresh(ev = null) {
    this.getGlobal();
    this.getTop5();
    // this.getNewest();
    if (ev) {
      ev.target.complete();
    }
  }

  async getGlobal() {
    this.mainData = await this.mainSrv.get();
  }

  async getTop5() {
    let index = 0;
    for await (const icountry of this.countryTop) {
      this.countryTop[index].data = await this.mainSrv.getByCodeCountry(icountry.code);
      index++;
    }
  }


  async getNewest() {
    const newx = await this.mainSrv.getNewest();
    this.newest = newx.reverse().filter((currentValue, index, arr) => {
      return index === 0 ? currentValue : null;
    });
  }

}
