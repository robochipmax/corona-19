import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomePageModule) },
  {
    path: 'home-detail/:country_code/:status',
    loadChildren: () => import('./home-detail/home-detail.module').then(m => m.HomeDetailPageModule)
  },
  {
    path: 'abouts',
    loadChildren: () => import('./abouts/abouts.module').then( m => m.AboutsPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
