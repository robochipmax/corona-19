import { DataModel } from './data_model';

export class CountryTopModel {
    name: string;
    code: string;
    data: DataModel;
};
