import { ModalCountryComponent } from './../modal-country/modal-country.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeDetailPageRoutingModule } from './home-detail-routing.module';

import { HomeDetailPage } from './home-detail.page';
import { MomentModule } from 'ngx-moment';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MomentModule,
    IonicModule,
    HomeDetailPageRoutingModule
  ],
  declarations: [HomeDetailPage, ModalCountryComponent],
  entryComponents: [
    ModalCountryComponent
  ]
})
export class HomeDetailPageModule { }
