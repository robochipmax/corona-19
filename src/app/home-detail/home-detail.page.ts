import { ModalCountryComponent } from './../modal-country/modal-country.component';
import { ModalController } from '@ionic/angular';
import { MainService } from './../services/main.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-detail',
  templateUrl: './home-detail.page.html',
  styleUrls: ['./home-detail.page.scss'],
})
export class HomeDetailPage implements OnInit {
  countryCode: any = '0';
  status: any = '0';
  items: any = [];
  title: any = 'Detail';
  isLoading = true;
  constructor(
    private snapRoute: ActivatedRoute,
    private mainSrv: MainService,
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {
    this.countryCode = this.snapRoute.snapshot.paramMap.get('country_code');
    this.status = this.snapRoute.snapshot.paramMap.get('status');
    this.title = this.countryCode === '0' ? 'Top 20' : this.countryCode;
  }

  ionViewDidEnter() {
    this.getData();
  }


  async getData() {
    this.isLoading = true;
    const x = await this.mainSrv.getDetail(this.countryCode, this.status);
    this.items = x.filter((currentVal, index) => {
      return index < 20 && this.countryCode === '0' ? currentVal : null;
    })
    this.isLoading = false;
  }

  async openCountry() {
    const modal = await this.modalCtrl.create({
      component: ModalCountryComponent,
      componentProps: { codeSelected: this.countryCode }
    });
    modal.present();

    modal.onDidDismiss().then((result) => {
      if (result.data) {
        if (this.countryCode !== result.data.code) {
          this.countryCode = result.data.code;
          this.title = this.countryCode === '0' ? 'Top 20' : this.countryCode;
          this.getData();
        }
      }
    })
  }

}
