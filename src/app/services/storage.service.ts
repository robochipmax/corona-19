import { Platform } from '@ionic/angular';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {

  constructor(
    private localStorage: Storage,
    private nativeStorage: NativeStorage,
    private platform: Platform
  ) {
  }

  private async getLocal(key: string) {
    return this.localStorage.get(key);
  }

  private async getNative(key: string) {
    return new Promise((resolve) => {
      this.nativeStorage.getItem(key).then((data: any) => {
        resolve(data);
      }).catch(err => resolve(''));
    });
  }

  private async setLocal(key: string, value: any) {
    return this.localStorage.set(key, value);
  }

  private async setNative(key, value) {
    value = typeof value === 'string' ? JSON.parse(value) : value;
    return this.nativeStorage.setItem(key, value);
  }


  private async removeLocal(key: string) {
    return this.localStorage.remove(key);
  }

  private async removeNative(key: string) {
    return this.nativeStorage.remove(key);
  }

  public async get(key: string) {
    return this.platform.is('cordova') ? this.getNative(key) : this.getLocal(key);
    // return this.getLocal(key);
  }

  public async set(key: string, value: any) {
    return this.platform.is('cordova') ? this.setNative(key, value) : this.setLocal(key, value);
    // return this.setLocal(key, value);
  }

  public async remove(key) {
    return this.platform.is('cordova') ? this.removeNative(key) : this.removeLocal(key);
    // return this.removeLocal(key);
  }
}
