import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network/ngx';
import { BehaviorSubject, Observable } from 'rxjs';
import { ToastController, Platform, LoadingController, ModalController } from '@ionic/angular';
export enum ConnectionStatus {
  Online,
  Offline
}
@Injectable({
  providedIn: 'root'
})
export class NetworkService {
  private mod: any;
  private status: BehaviorSubject<ConnectionStatus> = new BehaviorSubject(ConnectionStatus.Offline);
  // tslint:disable-next-line:max-line-length
  constructor(private network: Network, private modalCtrl: ModalController, private toastController: ToastController, private plt: Platform, private loadingCtrl: LoadingController) {
    this.plt.ready().then(() => {
      if (this.plt.is('cordova')) {
        this.initializeNetworkEvents();
        const status = this.network.type !== 'none' ? ConnectionStatus.Online : ConnectionStatus.Offline;
        this.status.next(status);
      }
    });
  }
  public initializeNetworkEvents() {
    this.network.onDisconnect().subscribe(() => {
      if (this.status.getValue() === ConnectionStatus.Online) {
        console.log('WE ARE OFFLINE');
        this.updateNetworkStatus(ConnectionStatus.Offline);
      }
    });
    this.network.onConnect().subscribe(() => {
      if (this.status.getValue() === ConnectionStatus.Offline) {
        console.log('WE ARE ONLINE');
        this.updateNetworkStatus(ConnectionStatus.Online);
      }
    });
  }
  private async updateNetworkStatus(status: ConnectionStatus) {
    this.status.next(status);
    const connection = status === ConnectionStatus.Offline ? 'Offline' : 'Online';
    const toast = this.toastController.create({
      message: 'You are now ' + connection,
      duration: 3000,
      color: status === ConnectionStatus.Offline ? 'danger' : 'success',
    });
    toast.then(xtoast => xtoast.present());
    // if (this.mod) {
    //   this.mod.dismiss();
    // }
    // this.mod = await this.modalCtrl.create({
    //   component: OfflineModalComponent,
    // });
    await this.mod.present();
    // if (ConnectionStatus.Offline === status) {
    //   this.loadingCtrl.dismiss();
    // }
    if (ConnectionStatus.Online) {
      this.mod.dismiss();
    }
  }
  public onNetworkChange(): Observable<ConnectionStatus> {
    return this.status.asObservable();
  }
  public getCurrentNetworkStatus(): ConnectionStatus {
    return this.status.getValue();
  }
}
