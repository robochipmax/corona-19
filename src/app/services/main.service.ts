import { environment } from './../../environments/environment';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MainService {
  private BASE_URL = environment.baseUrl;
  constructor(
    private apiSrv: ApiService
  ) { }

  get() {
    return this.apiSrv.getRequest(this.BASE_URL + 'api').toPromise();
  }

  getDetail(country_code: string = '0', status_code: string = '0') {
    let end_point = '';
    if (country_code !== '0') {
      end_point += 'countries/' + country_code.toLocaleLowerCase() + '/';
    }
    if (status_code !== '0') {
      end_point += status_code;
    }

    return this.apiSrv.getRequest(this.BASE_URL + 'api/' + end_point).toPromise();
  }

  getByCodeCountry(countryCode: string = '') {
    return this.apiSrv.getRequest(this.BASE_URL + 'api/countries/' + countryCode).toPromise();
  }

  getNewest() {
    return this.apiSrv.getRequest(this.BASE_URL + 'api/daily').toPromise();
  }
}
