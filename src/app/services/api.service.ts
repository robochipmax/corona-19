import { StorageService } from './storage.service';
import { Platform } from '@ionic/angular';

import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NetworkService, ConnectionStatus } from './network.service';
import { OfflineManagerService } from './offline-manager.service';
import {
  Observable,
  from,
  throwError
} from 'rxjs';
import {
  map,
  tap,
  catchError,
  retry
} from 'rxjs/operators';
import { File as FileX } from '@ionic-native/file/ngx';
import { environment } from './../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public httpOptions: any;
  public httpOptionsNative: any;
  public url = environment.baseUrl;

  constructor(
    private storageCtrl: StorageService,
    private networkSrv: NetworkService,
    private offlineSrv: OfflineManagerService,
    private httpCtrl: HttpClient,
    private fileCtrl: FileX,
    private platformCtrl: Platform
  ) {
    const contentHeader = {
      // 'Content-Type': 'multipart/form-data; charset=utf-8; boundary=' + Math.random().toString().substr(2),
    };
    this.httpOptions = {
      headers: new HttpHeaders(contentHeader)
    };
    this.httpOptionsNative = contentHeader;
  }

  toFixedFix(n, prec) {
    const k = Math.pow(10, prec);
    return '' + Math.round(n * k) / k;
  }

  // tslint:disable-next-line: variable-name
  number_format(number: string, decimals = 0, dec_point = ',', thousands_sep = '.') {
    // *     example: number_format(1234.56, 2, ',', ' ');
    // *     return: '1 234,56'
    number = (number + '').replace(',', '').replace(' ', '');
    const n = !isFinite(+number) ? 0 : +number;
    const prec = !isFinite(+decimals) ? 0 : Math.abs(decimals);
    const sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep;
    const dec = (typeof dec_point === 'undefined') ? '.' : dec_point;
    const s = (prec ? this.toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
      s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
      s[1] = s[1] || '';
      s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
  }

  // tslint:disable-next-line: ban-types
  public getMIMEtype(extn: any): String {
    const ext = extn.toLowerCase();

    const MIMETypes = {
      txt: 'text/plain',
      docx: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      doc: 'application/msword',
      pdf: 'application/pdf',
      jpg: 'image/jpeg',
      bmp: 'image/bmp',
      mp3: 'audio/mpeg',
      mp4: 'audio/mp4',
      png: 'image/png',
      xls: 'application/vnd.ms-excel',
      xlsx: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      rtf: 'application/rtf',
      ppt: 'application/vnd.ms-powerpoint',
      pptx: 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
    };
    return MIMETypes[ext];
  }
  public jsonToURLEncoded(jsonString: any): string {
    // tslint:disable-next-line: only-arrow-functions
    return Object.keys(jsonString).map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(jsonString[key]);
    }).join('&');
  }

  private handleError(error: HttpErrorResponse) {
    console.log(error);
    let xerror = error.error;
    if (typeof error.error === 'string' && (error.status === 404)) {
      xerror = JSON.parse(error.error);
    }
    // console.log(error);
    // this.offlineSrv.storeRequest(error.url, error.headers, error.);
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error('Backend returned code ' + error.status, xerror);
    }
    let message = xerror.message;
    if (error.status === -1) {
      message = xerror.error;
    }
    message = message || 'Something bad happened; please try again later.';

    // return an observable with a user-facing error message
    return new Promise(async (reject) => {
      reject(message);
    });
    // return throwError(message || 'Something bad happened; please try again later.');
  }
  private extractData(res: any): any {
    const body = res;
    return body || {};
  }
  async fileToBlob(uri) {
    // tslint:disable-next-line: variable-name
    const file_name = uri.substring(uri.lastIndexOf('/') + 1);
    // tslint:disable-next-line: variable-name
    const file_folder = uri.substring(0, uri.indexOf(file_name));
    const fileExtn = file_name.split('.').reverse()[0];
    if (fileExtn === '') {
      return uri;
    }
    // console.log(file_folder + "-" + file_name);
    const fileMIMEType = await this.getMIMEtype(fileExtn);
    return await this.fileCtrl.readAsDataURL(file_folder, file_name).then(async (fileData) => {
      const binary = atob(fileData.split(',')[1]);
      const array = [];
      let i = 0;
      const l = binary.length;
      for (; i < l; i++) {
        array.push(binary.charCodeAt(i));
      }
      const c = await new Blob([new Uint8Array(array)], { type: '${fileMIMEType}' });
      return await {
        content: c,
        type: fileMIMEType,
        name: file_name,
      };
    }).catch((err) => {
      console.error('ERROR : ' + uri, JSON.stringify(err));
    });
  }

  // tslint:disable-next-line: ban-types
  async createFormData(object: Object, form?: FormData, namespace?: string) {
    const formData = form || new FormData();

    for (const property in object) {
      if (!object.hasOwnProperty(property)) {
        continue;
      }
      const formKey = namespace ? `${namespace}[${property}]` : property;
      if (object[property] instanceof Date) {
        formData.append(formKey, object[property].toISOString());
      } else if (typeof object[property] === 'object' && !(object[property] instanceof File)) {
        await this.createFormData(object[property], formData, formKey);
      } else {
        // console.log(formKey + "=" + object[property]);
        if (typeof object[property] === 'string' && object[property].indexOf('file://') > -1) {
          // this.eventCtrl.publish('loading:show',{message:'Lampiran File Sedang di Proses.'});
          const urlS = await this.fileToBlob(object[property]);
          if (urlS) {
            formData.append(formKey, urlS.content, urlS.name);
          }
        } else {
          formData.append(formKey, object[property]);
        }
      }
    }
    return formData;
  }

  formToJSON(elem) {
    const output = {};
    elem.forEach(
      (value, key) => {
        // Check if property already exist
        if (Object.prototype.hasOwnProperty.call(output, key)) {
          let current = output[key];
          if (!Array.isArray(current)) {
            // If it's not an array, convert it to an array.
            current = output[key] = [current];
          }
          current.push(value); // Add the new value to the array.
        } else {
          output[key] = value;
        }
      }
    );
    return JSON.stringify(output);
  }

  getRequest(endpoint: string, forceRefresh: boolean = true): Observable<any> {
    // endpoint = this.url + endpoint;
    // console.log('Get Request URL', endpoint);
    let httpCall;
    // tslint:disable-next-line: no-string-literal
    // if (this.platformCtrl.is('cordova') && HTTP['installed']) {
    //   if (this.networkSrv.getCurrentNetworkStatus() === ConnectionStatus.Online) {
    //     // console.log('HTTP NATIVE');
    //     httpCall = this.httpNative.get(endpoint, {}, this.httpOptionsNative);
    //     // httpCall.then((res) => {
    //     //   console.log(res);
    //     // }).catch((err) => {
    //     //   console.error(err);
    //     // });
    //     httpCall = from(httpCall);
    //     return httpCall.pipe(
    //       map((res: any) => res),
    //       tap((res: any) => {
    //         this.setLocalData(endpoint, res);
    //       }),
    //       catchError(this.handleError)
    //     );
    //   } else {
    //     return throwError('Connection Error');
    //   }
    // } else {
    // console.log('HTTP COMMON');
    httpCall = this.httpCtrl.get(endpoint, this.httpOptions);
    return httpCall.pipe(
      retry(2),
      map((res: any) => res),
      // tap((res: any) => {
      //   this.setLocalData(endpoint, res);
      // }),
      catchError(this.handleError)
    );
    // }

  }

  async postRequest(endpoint: string, data: any, files: string = '', filename: any = 'filename') {
    let httpCall;
    const datax = await this.createFormData(data);
    // tslint:disable-next-line: no-string-literal
    // console.table('Post Request Data', this.formToJSON(datax));
    httpCall = this.httpCtrl.post(endpoint, datax, this.httpOptionsNative);
    httpCall = from(httpCall);
    return httpCall.pipe(
      retry(2),
      map((res: any) => res),
      catchError(this.handleError)
    ).toPromise();
  }
  // Save result of API requests
  private setLocalData(key: string, data: any) {
    this.storageCtrl.set(key, data);
  }
  // Get cached API result
  private getLocalData(key: string) {
    return this.storageCtrl.get(key);
  }

}
