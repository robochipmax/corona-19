import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private backgroundMode: BackgroundMode
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      if (this.platform.is('cordova')) {
        this.backgroundMode.setDefaults({
          hidden: true,
          icon: 'icon',
          resume: true,
          bigText: false,
          silent: true
        });
        this.statusBar.overlaysWebView(true);
        // this.statusBar.backgroundColorByHexString('#d7d8da');
        this.splashScreen.hide();
      }
    });
  }
}
