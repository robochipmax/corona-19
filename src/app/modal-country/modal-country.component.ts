import { StorageService } from './../services/storage.service';
import { ModalController, NavParams } from '@ionic/angular';
import { MainService } from './../services/main.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal-country',
  templateUrl: './modal-country.component.html',
  styleUrls: ['./modal-country.component.scss'],
})
export class ModalCountryComponent implements OnInit {
  itemsCountry: any = [];
  isLoading = true;
  codeSelected: any;
  constructor(
    private mainSrv: MainService,
    private modalCtrl: ModalController,
    private navParam: NavParams,
    private storage: StorageService
  ) { }

  ngOnInit() {
    this.codeSelected = this.navParam.get('codeSelected');
  }

  ionViewDidEnter() {
    this.getCountry();
  }

  async getCountry() {
    this.isLoading = true;
    const xc = [];
    this.storage.get('country').then(async (result: any) => {
      if (!result) {
        const axc = await this.mainSrv.getByCodeCountry();
        xc.push({
          name: 'All', iso2: '0'
        });

        this.itemsCountry = xc.concat(axc.countries);
        this.storage.set('country', this.itemsCountry);
      } else {
        this.itemsCountry = result;
      }
    })

    // console.log(this.itemsCountry);
    // let xc = JSON.stringify(axc.countries);
    // xc = xc.replace(/"/g, '');
    // let xcB: any = xc.substring(1, xc.length - 1).split(',');
    // this.itemsCountry = xcB.map((val) => {
    //   const x = val.split(":");
    //   return { name: x[0], code: x[1] };
    // });

    this.isLoading = false;

  }

  doClose() {
    this.modalCtrl.dismiss({
      code: this.codeSelected
    });
  }

  onUbah(event) {
    this.codeSelected = event.detail.value;
  }

}
