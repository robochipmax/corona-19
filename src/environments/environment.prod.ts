export const environment = {
  production: true,
  baseUrl: 'https://covid19.mathdro.id/',
  historyUrl: 'https://louislugas.github.io/covid_19_cluster/json/kasus-corona-indonesia.json'
};
