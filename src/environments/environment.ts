export const environment = {
  production: false,
  baseUrl: 'http://localhost:8100/',
  historyUrl: 'http://localhost:8100/covid_19_cluster/json/kasus-corona-indonesia.json'
};
